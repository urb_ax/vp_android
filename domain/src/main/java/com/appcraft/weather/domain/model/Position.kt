package com.appcraft.weather.domain.model

data class Position(
    val lat: Double,
    val lng: Double
)
