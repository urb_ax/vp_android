package com.appcraft.weather.domain.interactor.city

import com.appcraft.weather.domain.gateway.CityGateway
import com.appcraft.weather.domain.global.interactor.UseCaseWithParams
import com.appcraft.weather.domain.model.City
import com.appcraft.weather.domain.model.CityWeather
import kotlinx.coroutines.Dispatchers

class GetCityForecastUseCase(
    private val cityGateway: CityGateway
) : UseCaseWithParams<City, CityWeather>(Dispatchers.IO) {
    override suspend fun execute(parameters: City): CityWeather =
        cityGateway.getCityWeather(parameters)
}