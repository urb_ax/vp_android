package com.appcraft.weather.domain.interactor.city

import com.appcraft.weather.domain.gateway.CityGateway
import com.appcraft.weather.domain.global.interactor.UseCaseWithParams
import kotlinx.coroutines.Dispatchers

class SetCitySelectionUseCase(
    private val cityGateway: CityGateway
) : UseCaseWithParams<SetCitySelectionUseCase.Params, Unit>(Dispatchers.IO) {
    override suspend fun execute(parameters: Params): Unit =
        cityGateway.setCitySelection(parameters)

    data class Params(
        val id: Long,
        val selected: Boolean,
        val selectionTime: Long = 0
    )
}