package com.appcraft.weather.domain.interactor.city

import com.appcraft.weather.domain.gateway.CityGateway
import com.appcraft.weather.domain.global.interactor.UseCaseWithParams
import com.appcraft.weather.domain.model.CityWeather
import kotlinx.coroutines.Dispatchers

class GetSelectedCitiesForecastsUseCase(
    private val cityGateway: CityGateway
) : UseCaseWithParams<GetSelectedCitiesForecastsUseCase.Params, List<CityWeather>>(Dispatchers.IO) {
    override suspend fun execute(parameters: Params): List<CityWeather> =
        cityGateway.getSelectedCities(parameters)
            .map {
                cityGateway.getCityWeather(it)
            }

    data class Params(
        val offset: Long,
        val limit: Long
    )
}