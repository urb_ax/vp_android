package com.appcraft.weather.domain.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class City(
    val id: Long = 0,
    val name: String,
    val country: String,
    val lat: Double,
    val lng: Double,
    val selected: Boolean
) : Parcelable