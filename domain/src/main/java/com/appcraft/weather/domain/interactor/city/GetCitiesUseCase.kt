package com.appcraft.weather.domain.interactor.city

import com.appcraft.weather.domain.gateway.CityGateway
import com.appcraft.weather.domain.global.interactor.UseCaseWithParams
import com.appcraft.weather.domain.model.City
import kotlinx.coroutines.Dispatchers

class GetCitiesUseCase(
    private val cityGateway: CityGateway
) : UseCaseWithParams<GetCitiesUseCase.Params, List<City>>(Dispatchers.IO) {
    override suspend fun execute(parameters: Params): List<City> =
        cityGateway.getCities(parameters)

    data class Params(
        val query: String,
        val offset: Long,
        val limit: Long
    )
}