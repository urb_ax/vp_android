package com.appcraft.weather.domain.interactor.city

import com.appcraft.weather.domain.gateway.CityGateway
import com.appcraft.weather.domain.global.interactor.UseCase
import kotlinx.coroutines.Dispatchers

class InitCitiesUseCase(
    private val cityGateway: CityGateway
) : UseCase<Unit>(Dispatchers.IO) {
    override suspend fun execute(): Unit =
        cityGateway.initCities()
}