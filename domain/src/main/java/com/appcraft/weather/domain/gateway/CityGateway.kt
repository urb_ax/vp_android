package com.appcraft.weather.domain.gateway

import com.appcraft.weather.domain.interactor.city.GetCitiesUseCase
import com.appcraft.weather.domain.interactor.city.GetSelectedCitiesForecastsUseCase
import com.appcraft.weather.domain.interactor.city.SetCitySelectionUseCase
import com.appcraft.weather.domain.model.City
import com.appcraft.weather.domain.model.CityWeather
import com.appcraft.weather.domain.model.Position

interface CityGateway {
    suspend fun getCities(parameters: GetCitiesUseCase.Params): List<City>

    suspend fun getCityById(parameters: Long): City

    suspend fun getCityForPosition(parameters: Position): City

    suspend fun addCities(parameters: List<City>)

    suspend fun initCities()

    suspend fun setCitySelection(parameters: SetCitySelectionUseCase.Params)

    suspend fun getSelectedCities(parameters: GetSelectedCitiesForecastsUseCase.Params): List<City>

    suspend fun getCityWeather(parameters: City): CityWeather
}