package com.appcraft.weather.data.network

import com.appcraft.weather.data.network.model.CityWeatherModel
import retrofit2.http.GET
import retrofit2.http.Query

interface CommonNetwork {
    @GET("/data/2.5/onecall")
    suspend fun getCityWeather(
        @Query("lat") lat: Double,
        @Query("lon") lng: Double,
        @Query("exclude") exclude: String,
        @Query("appid") apiKey: String,
        @Query("units") units: String,
        @Query("lang") lang: String
    ): CityWeatherModel
}
