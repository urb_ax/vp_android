package com.appcraft.weather.data.network

import android.text.TextUtils
import com.appcraft.weather.data.preference.Preferences
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException

class HeaderInterceptor(private val prefs: Preferences) : Interceptor {
    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val requestBuilder = request.newBuilder()
        requestBuilder.removeHeader(AUTHORIZATION)
        if (!TextUtils.isEmpty(prefs.authToken.get())) {
            requestBuilder.addHeader(
                AUTHORIZATION,
                String.format(TOKEN_TEMPLATE, prefs.authToken.get())
            )
        }
        return chain.proceed(requestBuilder.build())
    }

    companion object {
        private const val AUTHORIZATION = "Authorization"
        private const val TOKEN_TEMPLATE = "JWT %s"
        private const val LANGUAGE = "Language"
    }
}
