package com.appcraft.weather.data.storage.dao

import androidx.room.*
import com.appcraft.weather.data.storage.entity.CityEntity

@Dao
interface CityDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun add(city: CityEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addAll(cities: List<CityEntity>)

    @Query("SELECT * FROM CityEntity WHERE id == :id")
    suspend fun get(id: Long): CityEntity

    @Query("SELECT * FROM CityEntity WHERE name LIKE '%' || :query || '%' LIMIT :limit OFFSET :offset")
    suspend fun findCitiesByName(query: String, limit: Long, offset: Long): List<CityEntity>

    @Query("SELECT * FROM CityEntity ORDER BY (ABS(lat - :lat) + ABS(lng - :lng)) ASC")
    suspend fun findCitiesByLocation(lat: Double, lng: Double): CityEntity

    @Query("UPDATE CityEntity SET selected = :selected, selectionTime = :selectionTime WHERE id = :id")
    suspend fun updateSelection(id: Long, selected: Boolean, selectionTime: Long)

    @Query("SELECT * FROM CityEntity WHERE selected ORDER BY selectionTime ASC LIMIT :limit OFFSET :offset")
    suspend fun getSelectedCities(limit: Long, offset: Long): List<CityEntity>

    suspend fun citiesTableIsEmpty(): Boolean
        = findCitiesByName(query = "", limit = 1, offset = 0).isEmpty()
}