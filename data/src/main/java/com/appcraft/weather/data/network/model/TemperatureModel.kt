package com.appcraft.weather.data.network.model

import com.google.gson.annotations.SerializedName

data class TemperatureModel(
    @SerializedName("morn") val morning: Float,
    @SerializedName("day") val day: Float,
    @SerializedName("eve") val evening: Float,
    @SerializedName("night") val night: Float
)