package com.appcraft.weather.data.storage.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.appcraft.weather.domain.model.City

@Entity
data class CityEntity(
    @PrimaryKey
    val id: Long = 0,
    val name: String,
    val country: String,
    val lat: Double,
    val lng: Double,
    val selected: Boolean = false,
    val selectionTime: Long = 0
)

fun CityEntity.toCity(): City = City(
    id = this.id,
    name = this.name,
    country = this.country,
    lat = this.lat,
    lng = this.lng,
    selected = this.selected
)

fun City.toCityEntity(): CityEntity = CityEntity(
    id = this.id,
    name = this.name,
    country = this.country,
    lat = this.lat,
    lng = this.lng
)
