package com.appcraft.weather.data.storage.entity

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import com.appcraft.weather.domain.model.WeatherForecast

@Entity(
    foreignKeys = [
        ForeignKey(
            entity = CityEntity::class,
            parentColumns = ["id"],
            childColumns = ["cityId"],
            onDelete = ForeignKey.CASCADE
        )
    ]
)
data class WeatherForecastEntity(
    @PrimaryKey(autoGenerate = true)
    val id: Long = 0,
    val cityId: Long,
    val timestamp: Long,
    val temperatureMorning: Float,
    val temperatureDay: Float,
    val temperatureEvening: Float,
    val temperatureNight: Float,
    val pressure: Int,
    val humidity: Int,
    val windSpeed: Float,
    val weatherType: String?,
    val weatherDescription: String?,
    val precipitation: Int,
    val isCurrent: Boolean = false
)

fun WeatherForecastEntity.toWeatherForecast(): WeatherForecast = WeatherForecast(
    cityId = this.cityId,
    timestamp = this.timestamp,
    temperatureMorning = this.temperatureMorning,
    temperatureDay = this.temperatureDay,
    temperatureEvening = this.temperatureEvening,
    temperatureNight = this.temperatureNight,
    pressure = this.pressure,
    humidity = this.humidity,
    windSpeed = this.windSpeed,
    weatherType = this.weatherType,
    weatherDescription = this.weatherDescription,
    precipitation = this.precipitation
)
