package com.appcraft.weather.data.storage

import androidx.room.Database
import androidx.room.RoomDatabase
import com.appcraft.weather.data.storage.dao.CityDao
import com.appcraft.weather.data.storage.dao.WeatherForecastDao
import com.appcraft.weather.data.storage.entity.CityEntity
import com.appcraft.weather.data.storage.entity.WeatherForecastEntity

@Database(
    entities = [
        CityEntity::class,
        WeatherForecastEntity::class
    ],
    version = 1
)
abstract class AppDb : RoomDatabase() {
    abstract fun getCityDao(): CityDao
    abstract fun getWeatherForecastDao(): WeatherForecastDao
}