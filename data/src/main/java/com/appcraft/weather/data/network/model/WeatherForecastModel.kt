package com.appcraft.weather.data.network.model

import com.appcraft.weather.data.storage.entity.WeatherForecastEntity
import com.google.gson.annotations.SerializedName

data class WeatherForecastModel(
    @SerializedName("dt") val timestamp: Long,
    @SerializedName("temp") val temperature: TemperatureModel,
    @SerializedName("pressure") val pressure: Int,
    @SerializedName("humidity") val humidity: Int,
    @SerializedName("wind_speed") val windSpeed: Float,
    @SerializedName("weather") val weatherType: List<WeatherTypeModel>,
    @SerializedName("pop") val precipitation: Float
)

fun WeatherForecastModel.toWeatherForecastEntity(cityId: Long) = WeatherForecastEntity(
    cityId = cityId,
    timestamp = this.timestamp * 1000,
    temperatureMorning = this.temperature.morning,
    temperatureDay = this.temperature.day,
    temperatureEvening = this.temperature.evening,
    temperatureNight = this.temperature.night,
    pressure = this.pressure,
    humidity = this.humidity,
    windSpeed = this.windSpeed,
    weatherType = this.weatherType.getOrNull(0)?.iconType,
    weatherDescription = this.weatherType.getOrNull(0)?.description,
    precipitation = (this.precipitation * 100).toInt(),
    isCurrent = false
)