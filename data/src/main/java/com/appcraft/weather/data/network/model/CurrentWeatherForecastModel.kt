package com.appcraft.weather.data.network.model

import com.appcraft.weather.data.storage.entity.WeatherForecastEntity
import com.google.gson.annotations.SerializedName

data class CurrentWeatherForecastModel(
    @SerializedName("dt") val timestamp: Long,
    @SerializedName("temp") val temperatureDay: Float,
    @SerializedName("pressure") val pressure: Int,
    @SerializedName("humidity") val humidity: Int,
    @SerializedName("wind_speed") val windSpeed: Float,
    @SerializedName("weather") val weatherType: List<WeatherTypeModel>
)

fun CurrentWeatherForecastModel.toWeatherForecastEntity(cityId: Long) = WeatherForecastEntity(
    cityId = cityId,
    timestamp = this.timestamp * 1000,
    temperatureMorning = this.temperatureDay,
    temperatureDay = this.temperatureDay,
    temperatureEvening = this.temperatureDay,
    temperatureNight = this.temperatureDay,
    pressure = this.pressure,
    humidity = this.humidity,
    windSpeed = this.windSpeed,
    weatherType = this.weatherType.getOrNull(0)?.iconType,
    weatherDescription = this.weatherType.getOrNull(0)?.description,
    precipitation = 0,
    isCurrent = true
)