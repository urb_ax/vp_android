package com.appcraft.weather.data.network.model

import com.appcraft.weather.data.storage.entity.CityEntity
import com.google.gson.annotations.SerializedName

data class CityModel(
    @SerializedName("id") val id: Long,
    @SerializedName("name") val name: String,
    @SerializedName("country") val country: String,
    @SerializedName("coord") val coordinates: CoordinatesModel
)

fun CityModel.toCityEntity(): CityEntity = CityEntity(
    id = this.id,
    name = this.name,
    country = this.country,
    lat = this.coordinates.lat,
    lng = this.coordinates.lng
)