package com.appcraft.weather.data.gateway

import android.content.Context
import com.appcraft.weather.data.R
import com.appcraft.weather.data.network.CommonNetwork
import com.appcraft.weather.data.network.model.CityModel
import com.appcraft.weather.data.network.model.toCityEntity
import com.appcraft.weather.data.network.model.toWeatherForecastEntity
import com.appcraft.weather.data.storage.dao.CityDao
import com.appcraft.weather.data.storage.dao.WeatherForecastDao
import com.appcraft.weather.data.storage.entity.toCity
import com.appcraft.weather.data.storage.entity.toCityEntity
import com.appcraft.weather.data.storage.entity.toWeatherForecast
import com.appcraft.weather.domain.gateway.CityGateway
import com.appcraft.weather.domain.interactor.city.GetCitiesUseCase
import com.appcraft.weather.domain.interactor.city.GetSelectedCitiesForecastsUseCase
import com.appcraft.weather.domain.interactor.city.SetCitySelectionUseCase
import com.appcraft.weather.domain.model.City
import com.appcraft.weather.domain.model.CityWeather
import com.appcraft.weather.domain.model.Position
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.io.IOException
import java.io.InputStream
import java.lang.Exception
import java.nio.charset.Charset
import kotlin.math.abs

class CityGatewayImpl(
    private val context: Context,
    private val cityDao: CityDao,
    private val weatherForecastDao: WeatherForecastDao,
    private val commonNetwork: CommonNetwork
) : CityGateway {
    override suspend fun getCities(parameters: GetCitiesUseCase.Params): List<City> =
        cityDao.findCitiesByName(
            query = parameters.query,
            limit = parameters.limit,
            offset = parameters.offset
        ).map { it.toCity() }

    override suspend fun getCityById(parameters: Long): City =
        cityDao.get(parameters).toCity()

    override suspend fun getCityForPosition(parameters: Position): City =
        cityDao.findCitiesByLocation(
            lat = parameters.lat,
            lng = parameters.lng
        ).toCity()

    override suspend fun addCities(parameters: List<City>) =
        cityDao.addAll(
            parameters.map { it.toCityEntity() }
        )

    override suspend fun initCities() {
        if (cityDao.citiesTableIsEmpty()) {
            val cityModels = mutableListOf<CityModel>()
            CITIES_FILE_NAMES.forEach { fileName ->
                cityModels.addAll(
                    Gson().fromJson<List<CityModel>>(
                        getCitiesJson(fileName),
                        object : TypeToken<List<CityModel>>() { }.type
                    )
                )
            }
            val cities = cityModels
                // removing duplicated cities from source json
                .sortedBy { it.name }
                .toMutableList()
                .apply {
                    var index = 0
                    while (index < (size - 1)) {
                        val currentCity = get(index)
                        val nextCity = get(index + 1)
                        if ((currentCity.name == nextCity.name)
                            && (abs(currentCity.coordinates.lat - nextCity.coordinates.lat) < LAT_LNG_THRESHOLD)
                            && (abs(currentCity.coordinates.lng - nextCity.coordinates.lng) < LAT_LNG_THRESHOLD)
                        ) {
                            removeAt(index + 1)
                        } else {
                            index++
                        }
                    }
                }
                .map { it.toCityEntity() }
            cityDao.addAll(cities)

            cityDao.updateSelection(CITY_ID_MOSCOW, true, 0)
            cityDao.updateSelection(CITY_ID_MINSK, true, 1)
        }
    }

    private fun getCitiesJson(fileName: String): String? = try {
            val inputStream: InputStream = context.assets.open(fileName)
            val size: Int = inputStream.available()
            val buffer = ByteArray(size)
            inputStream.read(buffer)
            inputStream.close()
            String(buffer, Charset.forName("UTF-8"))
        } catch (e: IOException) {
            e.printStackTrace()
            null
        }

    override suspend fun setCitySelection(parameters: SetCitySelectionUseCase.Params) =
        cityDao.updateSelection(
            id = parameters.id,
            selected = parameters.selected,
            selectionTime = parameters.selectionTime
        )

    override suspend fun getSelectedCities(parameters: GetSelectedCitiesForecastsUseCase.Params): List<City> =
        cityDao.getSelectedCities(
            limit = parameters.limit,
            offset = parameters.offset
        ).map { it.toCity() }

    override suspend fun getCityWeather(parameters: City): CityWeather {
        val dayBorderTimestamp = System.currentTimeMillis() - ONE_DAY_MS
        val hourBorderTimestamp = System.currentTimeMillis() - ONE_HOUR_MS

        var forecasts = weatherForecastDao
            .getForecasts(parameters.id)
            .let { data ->
                data.firstOrNull { it.timestamp < dayBorderTimestamp }?.let {
                    weatherForecastDao.deleteOldForecasts(dayBorderTimestamp)
                    data.filter { it.timestamp >= dayBorderTimestamp }
                } ?: data
            }
        var currentForecast = forecasts.firstOrNull { it.isCurrent }
        forecasts = forecasts.filter { it.id != currentForecast?.id }

        if ((currentForecast?.timestamp ?: 0) < hourBorderTimestamp) {
            try {
                val weather = commonNetwork.getCityWeather(
                    apiKey = context.getString(R.string.open_weather_map_api_key),
                    lat = parameters.lat,
                    lng = parameters.lng,
                    exclude = OWM_EXCLUDE,
                    units = OWM_UNITS,
                    lang = OWM_LANG
                )
                currentForecast = weather.current.toWeatherForecastEntity(parameters.id)
                forecasts = weather.daily.map { it.toWeatherForecastEntity(parameters.id) }

                weatherForecastDao.deleteForecasts(parameters.id)
                weatherForecastDao.add(currentForecast)
                weatherForecastDao.addAll(forecasts)
            } catch (e: Exception) {
                forecasts.minByOrNull { it.timestamp }?.let { oldestForecast ->
                    if (((currentForecast?.timestamp ?: 0) + ONE_DAY_MS) < oldestForecast.timestamp) {
                        currentForecast = oldestForecast
                        forecasts = forecasts.filter { it.id != currentForecast?.id }
                    }
                }
            }
        }

        return CityWeather(
            city = parameters,
            currentWeather = currentForecast?.toWeatherForecast(),
            forecasts = forecasts.map { it.toWeatherForecast() }
        )
    }

    companion object {
        private val CITIES_FILE_NAMES = listOf(
            "city.list.1.json",
            "city.list.2.json",
            "city.list.3.json",
            "city.list.4.json"
        )
        private const val LAT_LNG_THRESHOLD = 0.1
        private const val ONE_DAY_MS = 86_400_000L
        private const val ONE_HOUR_MS = 3_600_000L
        private const val OWM_EXCLUDE = "minutely,hourly,alerts"
        private const val OWM_UNITS = "metric"
        private const val OWM_LANG = "ru"
        private const val CITY_ID_MOSCOW = 524894L
        private const val CITY_ID_MINSK = 625144L
    }
}