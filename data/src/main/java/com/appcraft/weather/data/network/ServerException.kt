package com.appcraft.weather.data.network

class ServerException(val code: Int, message: String?) : Exception(message)
