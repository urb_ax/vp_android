package com.appcraft.weather.data.preference

import com.appcraft.weather.domain.model.Language
import com.f2prateek.rx.preferences2.RxSharedPreferences

class Preferences(preferences: RxSharedPreferences) {
    val language: CachedPreference<String> =
        CachedPreference(preferences.getString(LANGUAGE, Language.EN.tag))
    val authToken: CachedPreference<String> =
        CachedPreference(preferences.getString(AUTH_TOKEN))

    companion object {
        private const val LANGUAGE = "LANGUAGE"
        private const val AUTH_TOKEN = "AUTH_TOKEN"
    }
}
