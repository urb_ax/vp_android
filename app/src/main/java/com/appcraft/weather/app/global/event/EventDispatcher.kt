package com.appcraft.weather.app.global.event

import android.os.Handler
import com.appcraft.weather.app.utils.EventType
import java.util.*
import kotlin.collections.ArrayList

class EventDispatcher {
    private val eventListeners = HashMap<EventType, MutableList<EventListener>>()

    fun addEventListener(eventType: EventType, listener: EventListener): EventListener {
        if (eventListeners[eventType] == null) {
            eventListeners[eventType] = ArrayList()
        }

        val list = eventListeners[eventType]
        list?.add(listener)

        return listener
    }

    fun removeEventListener(listener: EventListener) = eventListeners
        .filter { it.value.size > 0 }
        .forEach { it.value.remove(listener) }

    fun sendEvent(eventType: EventType, data: Any? = null) {
        eventListeners
            .filter { it.key == eventType && it.value.size > 0 }
            .forEach {
                it.value.forEach { listener ->
                    Handler().post {
                        listener.onEvent(
                            eventType,
                            data
                        )
                    }
                }
            }
    }

    interface EventListener {
        fun onEvent(eventType: EventType, data: Any?)
    }
}