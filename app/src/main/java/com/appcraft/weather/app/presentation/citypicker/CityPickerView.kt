package com.appcraft.weather.app.presentation.citypicker

import com.appcraft.weather.app.global.presentation.PagerView
import com.appcraft.weather.domain.model.City
import moxy.viewstate.strategy.AddToEndSingleTagStrategy
import moxy.viewstate.strategy.StateStrategyType
import moxy.viewstate.strategy.alias.AddToEndSingle

interface CityPickerView : PagerView {
    @StateStrategyType(value = AddToEndSingleTagStrategy::class, tag = LOCATION_PROGRESS_TAG)
    fun showLocationProgress()

    @StateStrategyType(value = AddToEndSingleTagStrategy::class, tag = LOCATION_PROGRESS_TAG)
    fun hideLocationProgress()

    @AddToEndSingle
    fun populateData(data: List<City>, suggestedCity: City?)

    companion object {
        private const val LOCATION_PROGRESS_TAG = "LOCATION_PROGRESS_TAG"
    }
}