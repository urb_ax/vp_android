package com.appcraft.weather.app.ui.fragment.main

import android.graphics.PointF
import android.media.MediaPlayer
import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.TranslateAnimation
import androidx.core.os.bundleOf
import com.appcraft.weather.app.BuildConfig
import com.appcraft.weather.app.R
import com.appcraft.weather.app.global.ui.fragment.BaseFragment
import com.appcraft.weather.app.global.utils.addSystemWindowInsetToMargin
import com.appcraft.weather.app.global.utils.vibrate
import com.appcraft.weather.app.presentation.main.SettingsPresenter
import com.appcraft.weather.app.presentation.main.SettingsView
import kotlinx.android.synthetic.main.fragment_settings.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import moxy.presenter.InjectPresenter
import pro.appcraft.lib.utils.common.setVisible
import java.util.*

class SettingsFragment : BaseFragment(), SettingsView {
    @InjectPresenter
    lateinit var presenter: SettingsPresenter

    private val flyingIconPath: MutableList<PointF> = mutableListOf()
    private var flyingIconCurrentPointIndex = 0

    override val contentView = R.layout.fragment_settings

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        content.addSystemWindowInsetToMargin(top = true, bottom = true)
        initToolbar()
        updateVersion()
        initOnClickListeners()

        view.post {
            initFlyingIcon()
        }
    }

    private fun initToolbar() {
        layoutToolbar.apply {
            textViewToolbarHeader.setText(R.string.about_app)
            buttonToolbarLeft.setVisible()
            buttonToolbarLeft.setImageResource(R.drawable.ic_back)
            buttonToolbarLeft.setOnClickListener {
                onBackPressed()
            }
        }
    }

    @Suppress("ConstantConditionIf")
    private fun updateVersion() {
        val buildType = if (BuildConfig.BUILD_TYPE == "release") "" else BuildConfig.BUILD_TYPE
        versionTextView.text = String.format(
            Locale.getDefault(), "%s %s (%d) %s",
            getString(R.string.version), BuildConfig.VERSION_NAME, BuildConfig.VERSION_CODE, buildType
        )
    }

    private fun initOnClickListeners() {
        sendEmailButton.setOnClickListener {
            val email = getString(R.string.support_email)
            val subject = getString(R.string.email_subject_template, Build.VERSION.RELEASE)
            presenter.onSendEmail(email, subject)
        }
    }

    private fun initFlyingIcon() {
        val width = requireView().width
        val height = requireView().height - containerFlyingIcon.height

        if ((width <= 0) || (height <= 0)) {
            return
        }

        flyingIconPath.clear()
        for (level in 0..(FLYING_ICON_ADDITIONAL_HEIGHTS + 1)) {
            val isLeftBorder = level % 2 == 1
            val xMultiplier = if (isLeftBorder) FLYING_ICON_LEFT_BORDER else FLYING_ICON_RIGHT_BORDER
            val yMultiplier = (FLYING_ICON_TOP_BORDER
                    + (FLYING_ICON_BOTTOM_BORDER - FLYING_ICON_TOP_BORDER)
                    * level / (FLYING_ICON_ADDITIONAL_HEIGHTS + 1))
            flyingIconPath.add(
                PointF(
                    (width * xMultiplier - (if (isLeftBorder) containerFlyingIcon.width else 0)).toFloat(),
                    (height * yMultiplier).toFloat()
                )
            )
        }
        for (level in (FLYING_ICON_ADDITIONAL_HEIGHTS + 1) downTo 0) {
            val isLeftBorder = level % 2 == 0
            val xMultiplier = if (isLeftBorder) FLYING_ICON_LEFT_BORDER else FLYING_ICON_RIGHT_BORDER
            val yMultiplier = (FLYING_ICON_TOP_BORDER
                    + (FLYING_ICON_BOTTOM_BORDER - FLYING_ICON_TOP_BORDER)
                    * level / (FLYING_ICON_ADDITIONAL_HEIGHTS + 1))
            flyingIconPath.add(
                PointF(
                    (width * xMultiplier - (if (isLeftBorder) containerFlyingIcon.width else 0)).toFloat(),
                    (height * yMultiplier).toFloat()
                )
            )
        }

        var mediaPlayer: MediaPlayer? = null
        imageViewFlyingIcon.setOnClickListener {
            requireContext().vibrate(VIBRATION_DURATION_MS)

            if (mediaPlayer?.isPlaying != true) {
                mediaPlayer = MediaPlayer.create(requireContext(), R.raw.thunder).apply {
                    setOnCompletionListener {
                        it.reset()
                        it.release()
                        mediaPlayer = null
                    }
                }
                mediaPlayer?.start()
            }
        }

        startFlyingIconAnimation()
    }

    private fun startFlyingIconAnimation() {
        flyingIconCurrentPointIndex = 0
        val destination = flyingIconPath[flyingIconCurrentPointIndex]

        containerFlyingIcon?.animate()
            ?.setDuration(0)
            ?.setStartDelay(0)
            ?.x(destination.x)
            ?.y(destination.y)
            ?.withEndAction {
                containerFlyingIcon?.setVisible()
                nextFlyingIconAnimation()
            }
            ?.start()

        val tremblingHeight = (imageViewFlyingIcon?.height?.toFloat() ?: 1f) * FLYING_ICON_TREMBLING_AMPLITUDE
        imageViewFlyingIcon?.startAnimation(
            TranslateAnimation(0f, 0f, tremblingHeight, -tremblingHeight).apply {
                duration = FLYING_ICON_TREMBLING_DURATION_MS
                repeatMode = TranslateAnimation.REVERSE
                repeatCount = TranslateAnimation.INFINITE
                interpolator = AccelerateDecelerateInterpolator()
            }
        )
    }

    private fun nextFlyingIconAnimation() {
        flyingIconCurrentPointIndex++
        if (flyingIconCurrentPointIndex >= flyingIconPath.size) {
            flyingIconCurrentPointIndex = 0
        }
        val destination = flyingIconPath[flyingIconCurrentPointIndex]

        containerFlyingIcon?.scaleX = if (flyingIconCurrentPointIndex % 2 == 0) 1f else -1f
        containerFlyingIcon?.animate()
            ?.setDuration(FLYING_ICON_LINE_TIME_MS)
            ?.setStartDelay(FLYING_ICON_PAUSE_TIME_MS)
            ?.x(destination.x)
            ?.y(destination.y)
            ?.withEndAction {
                nextFlyingIconAnimation()
            }
            ?.start()
    }

    companion object {
        private const val FLYING_ICON_LEFT_BORDER = -0.1
        private const val FLYING_ICON_RIGHT_BORDER = 1.1
        private const val FLYING_ICON_TOP_BORDER = 0.05
        private const val FLYING_ICON_BOTTOM_BORDER = 0.99
        private const val FLYING_ICON_ADDITIONAL_HEIGHTS = 2
        private const val FLYING_ICON_TREMBLING_AMPLITUDE = 0.6f
        private const val FLYING_ICON_TREMBLING_DURATION_MS = 750L
        private const val FLYING_ICON_LINE_TIME_MS = 7_000L
        private const val FLYING_ICON_PAUSE_TIME_MS = 2_000L
        private const val VIBRATION_DURATION_MS = 250L

        fun newInstance(): SettingsFragment {
            return SettingsFragment()
                .apply {
                    arguments = bundleOf()
                }
        }
    }
}
