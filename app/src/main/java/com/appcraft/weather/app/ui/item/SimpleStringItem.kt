package com.appcraft.weather.app.ui.item

import android.view.View
import androidx.appcompat.widget.AppCompatTextView
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.items.AbstractItem
import com.appcraft.weather.app.R

class SimpleStringItem(val string: String) : AbstractItem<SimpleStringItem.ViewHolder>() {
    override val type: Int = R.id.simpleStringItem

    override val layoutRes: Int = R.layout.item_simple_string

    override fun getViewHolder(v: View): ViewHolder = ViewHolder(v)

    class ViewHolder(view: View) : FastAdapter.ViewHolder<SimpleStringItem>(view) {
        private val textViewName: AppCompatTextView = view.findViewById(R.id.textViewName)

        override fun bindView(item: SimpleStringItem, payloads: List<Any>) {
            textViewName.text = item.string
        }

        override fun unbindView(item: SimpleStringItem) {}
    }
}