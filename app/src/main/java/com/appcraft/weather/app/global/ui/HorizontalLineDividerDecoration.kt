package com.appcraft.weather.app.global.ui

import android.content.Context
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import androidx.annotation.DimenRes
import androidx.recyclerview.widget.RecyclerView
import com.appcraft.weather.app.R

class HorizontalLineDividerDecoration(
    context: Context,
    @DimenRes size: Int,
    @DimenRes padding: Int
) : RecyclerView.ItemDecoration() {
    private val sizePx: Int = context.resources.getDimensionPixelSize(size)
    private val paddingPx: Int = context.resources.getDimensionPixelSize(padding)
    private var dividerDrawable: Drawable? = context.getDrawable(R.drawable.bg_settings_divider)

    override fun onDrawOver(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        dividerDrawable?.let { drawable ->
            val left = paddingPx
            val right = parent.width - paddingPx

            val childCount = parent.childCount
            for (i in 0 until childCount) {
                val child = parent.getChildAt(i)
                val params =
                    child.layoutParams as RecyclerView.LayoutParams
                val top = child.bottom + params.bottomMargin
                val bottom: Int = top + sizePx
                drawable.setBounds(left, top, right, bottom)
                drawable.draw(c)
            }
        }
    }
}