package com.appcraft.weather.app.presentation.main

import com.appcraft.weather.app.global.presentation.PagerView
import com.appcraft.weather.domain.model.City
import com.appcraft.weather.domain.model.CityWeather
import moxy.viewstate.strategy.AddToEndSingleTagStrategy
import moxy.viewstate.strategy.StateStrategyType
import moxy.viewstate.strategy.alias.AddToEndSingle

interface MainView : PagerView {
    @StateStrategyType(value = AddToEndSingleTagStrategy::class, tag = PROGRESS_TAG)
    fun showProgress()

    @StateStrategyType(value = AddToEndSingleTagStrategy::class, tag = PROGRESS_TAG)
    fun hideProgress()

    @AddToEndSingle
    fun populateData(data: List<CityWeather>)

    companion object {
        private const val PROGRESS_TAG = "PROGRESS_TAG"
    }
}