package com.appcraft.weather.app

import android.app.Application
import appComponent
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class App : Application() {
    override fun onCreate() {
        super.onCreate()

        initKoin()
    }

//    private fun initWebView() {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
//            val processName = getProcessName()
//            if (packageName != processName) {
//                WebView.setDataDirectorySuffix(processName)
//            }
//        }
//    }

    private fun initKoin() {
        startKoin {
            if (BuildConfig.DEBUG)
                androidLogger()

            androidContext(this@App)

            modules(appComponent)
        }
    }
}
