package com.appcraft.weather.app.ui

import android.app.ActivityManager
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.BitmapFactory
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.TextUtils
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.appcraft.weather.app.R
import com.appcraft.weather.app.Screens
import com.appcraft.weather.app.global.notifier.Notifier
import com.appcraft.weather.app.global.notifier.SystemMessage
import com.appcraft.weather.app.global.ui.activity.BaseActivity
import com.appcraft.weather.app.global.ui.fragment.FlowFragment
import com.appcraft.weather.app.global.utils.addSystemWindowInsetToMargin
import com.appcraft.weather.app.presentation.AppPresenter
import com.appcraft.weather.app.presentation.AppView
import com.appcraft.weather.domain.global.CoroutineProvider
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import moxy.presenter.InjectPresenter
import org.koin.android.ext.android.inject
import pro.appcraft.lib.navigation.AppNavigator
import pro.appcraft.lib.navigation.AppRouter
import pro.appcraft.lib.utils.dialog.AlertDialogAction
import pro.appcraft.lib.utils.dialog.AlertDialogType
import pro.appcraft.lib.utils.dialog.showAlertDialog
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.commands.Command
import ru.terrakok.cicerone.commands.Forward
import ru.terrakok.cicerone.commands.Replace

class AppActivity : BaseActivity(), AppView {
    @InjectPresenter
    lateinit var presenter: AppPresenter
    private val navigatorHolder: NavigatorHolder by inject()
    private val notifier: Notifier by inject()
    private val coroutineProvider: CoroutineProvider by inject()
    private val router: AppRouter by inject()

    private var notificationChannel: ReceiveChannel<SystemMessage>? = null

    private val navigator = object : AppNavigator(this, supportFragmentManager, R.id.container) {
        private var doubleBackToExitPressedOnce: Boolean = false

        override fun setupFragmentTransaction(
            command: Command,
            currentFragment: Fragment?,
            nextFragment: Fragment?,
            fragmentTransaction: FragmentTransaction
        ) {
            //fix incorrect order lifecycle callback of MainFlowFragment
            fragmentTransaction.setReorderingAllowed(true)

            if (command is Forward || command is Replace) {
                currentFragment?.exitTransition = null
                nextFragment?.enterTransition = null
            }
        }

        override fun activityBack() {
            if (doubleBackToExitPressedOnce) {
                super.activityBack()
                return
            }
            doubleBackToExitPressedOnce = true
            notifier.sendMessage(R.string.double_back_to_exit)
            val exitDuration = resources.getInteger(R.integer.exit_duration)
            Handler(Looper.getMainLooper()).postDelayed({ doubleBackToExitPressedOnce = false }, exitDuration.toLong())
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity)
        initWindowFlags()
        updateTaskDescription()

        if (savedInstanceState == null) {
            router.newRootFlow(Screens.SplashFlow())
        } else {
            window.setBackgroundDrawableResource(R.color.colorBackground)
        }
    }

    private fun initWindowFlags() {
        var flags = window.decorView.systemUiVisibility
        flags = flags or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
        flags = flags or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        flags = flags or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            flags = flags or View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR
//        }
        window.decorView.systemUiVisibility = flags
    }

    @Suppress("DEPRECATION")
    private fun updateTaskDescription() {
        val taskDesc = if (Build.VERSION.SDK_INT >= 28) {
            ActivityManager.TaskDescription(
                resources.getString(R.string.app_name),
                R.mipmap.ic_launcher,
                ContextCompat.getColor(this, R.color.colorTextWhite)
            )
        } else
            ActivityManager.TaskDescription(
                resources.getString(R.string.app_name),
                BitmapFactory.decodeResource(resources, R.mipmap.ic_launcher),
                ContextCompat.getColor(this, R.color.colorTextWhite)
            )

        setTaskDescription(taskDesc)
    }

    override fun onStart() {
        super.onStart()
        subscribeOnSystemMessages()
    }

    private fun subscribeOnSystemMessages() {
        notificationChannel = notifier.subscribe()
        coroutineProvider.scopeMainImmediate.launch {
            notificationChannel?.receiveAsFlow()?.collect(::onNextMessageNotify)
        }
    }

    override fun onResumeFragments() {
        super.onResumeFragments()
        navigatorHolder.setNavigator(navigator)
    }

    private fun onNextMessageNotify(systemMessage: SystemMessage) {
        val text = if (systemMessage.textRes == null) {
            systemMessage.text ?: return
        } else {
            getString(systemMessage.textRes)
        }
        val actionText = if (systemMessage.actionTextRes == null) {
            systemMessage.actionText ?: ""
        } else {
            getString(systemMessage.actionTextRes)
        }

        when (systemMessage.type) {
            SystemMessage.Type.BAR -> {
                showBarMessage(text, systemMessage.level)
            }
            SystemMessage.Type.ALERT -> {
                showAlertMessage(text)
            }
            SystemMessage.Type.ACTION -> {
                showActionMessage(
                    text,
                    actionText,
                    systemMessage.actionCallback,
                    systemMessage.level
                )
            }
        }
    }

    private fun showBarMessage(
        text: String,
        level: SystemMessage.Level
    ) {
        if (TextUtils.isEmpty(text)) {
            return
        }

        val backgroundResource = if (level == SystemMessage.Level.ERROR) R.drawable.bg_rounded_orange else R.drawable.bg_rounded_primary

        val snackbar = Snackbar.make(findViewById(R.id.snackBarContainer), text, Snackbar.LENGTH_LONG)
        val snackView = snackbar.view
        val snackbarTextView = snackView.findViewById<TextView>(R.id.snackbar_text)
        snackbarTextView.isSingleLine = false

        snackView.addSystemWindowInsetToMargin(
            top = true,
            bottom = true,
            topOffset = resources.getDimension(R.dimen.baseline_grid_small).toInt(),
            bottomOffset = resources.getDimension(R.dimen.baseline_grid_small).toInt()
        )
        snackView.requestApplyInsets()

        snackbarTextView.setTextColor(ContextCompat.getColor(this, R.color.colorTextWhite))
        snackView.setBackgroundResource(backgroundResource)

        snackbar.show()
    }

    private fun showAlertMessage(text: String) {
        if (TextUtils.isEmpty(text)) {
            return
        }

        showAlertDialog(
            type = AlertDialogType.ALERT_VERTICAL_1_OPTION_NO_ACCENT,
            message = text,
            cancellable = true,
            actions = listOf(
                AlertDialogAction(getString(R.string.okay)) {
                    it.dismiss()
                }
            )
        )
    }

    private fun showActionMessage(
        text: String,
        action: String?,
        actionCallback: (() -> Unit?)?,
        level: SystemMessage.Level
    ) {
        if (TextUtils.isEmpty(text)) {
            return
        }

        val backgroundResource =
            if (level == SystemMessage.Level.ERROR) R.drawable.bg_rounded_red else R.drawable.bg_rounded_primary

        val snackbar =
            Snackbar.make(findViewById(R.id.snackBarContainer), text, Snackbar.LENGTH_LONG)
        val snackView = snackbar.view
        val snackbarTextView = snackView.findViewById<TextView>(R.id.snackbar_text)
        snackbarTextView.isSingleLine = false

        snackView.addSystemWindowInsetToMargin(
            top = true,
            bottom = true,
            topOffset = resources.getDimension(R.dimen.baseline_grid_small).toInt(),
            bottomOffset = resources.getDimension(R.dimen.baseline_grid_small).toInt()
        )
        snackView.requestApplyInsets()

        snackbarTextView.setTextColor(ContextCompat.getColor(this, R.color.colorTextWhite))
        snackView.setBackgroundResource(backgroundResource)

        if (!TextUtils.isEmpty(action) && actionCallback != null) {
            snackbar.setAction(action) {
                actionCallback.invoke()
            }
            val button = snackView.findViewById<Button>(R.id.snackbar_action)
            button.setTextColor(ContextCompat.getColor(this, R.color.colorTextWhite))
            button.backgroundTintList =
                ColorStateList.valueOf(ContextCompat.getColor(this, R.color.colorAccent))
        }

        snackbar.show()
    }

    override fun onPause() {
        navigatorHolder.removeNavigator()
        super.onPause()
    }

    override fun onStop() {
        unsubscribeOnSystemMessages()
        super.onStop()
    }

    private fun unsubscribeOnSystemMessages() {
        notificationChannel?.cancel()
        notificationChannel = null
    }

    override fun onResume() {
        super.onResume()
        currentFocus?.clearFocus()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val fragment = supportFragmentManager.findFragmentById(R.id.container)

        if (fragment is FlowFragment) {
            val currentFragment = fragment.getCurrentFragment()
            currentFragment?.onActivityResult(requestCode, resultCode, data)
        }
    }
}
