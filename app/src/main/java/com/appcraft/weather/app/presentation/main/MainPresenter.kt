package com.appcraft.weather.app.presentation.main

import com.appcraft.weather.app.Screens
import com.appcraft.weather.app.global.event.EventDispatcher
import com.appcraft.weather.app.global.notifier.Notifier
import com.appcraft.weather.app.global.pagination.OffsetPaginator
import com.appcraft.weather.app.global.presentation.BasePresenter
import com.appcraft.weather.app.global.presentation.DefaultPaginatorCallback
import com.appcraft.weather.app.global.presentation.ErrorHandler
import com.appcraft.weather.app.utils.CityPickerResult
import com.appcraft.weather.app.utils.EventType
import com.appcraft.weather.domain.global.CoroutineProvider
import com.appcraft.weather.domain.global.interactor.UseCaseResult
import com.appcraft.weather.domain.interactor.city.GetCityForecastUseCase
import com.appcraft.weather.domain.interactor.city.GetSelectedCitiesForecastsUseCase
import com.appcraft.weather.domain.interactor.city.SetCitySelectionUseCase
import com.appcraft.weather.domain.model.City
import com.appcraft.weather.domain.model.CityWeather
import kotlinx.coroutines.launch
import moxy.InjectViewState
import org.koin.core.component.inject

@InjectViewState
class MainPresenter : BasePresenter<MainView>(), EventDispatcher.EventListener {
    private val coroutineProvider: CoroutineProvider by inject()
    private val eventDispatcher: EventDispatcher by inject()
    private val errorHandler: ErrorHandler by inject()
    private val notifier: Notifier by inject()
    private val getSelectedCitiesForecastsUseCase: GetSelectedCitiesForecastsUseCase by inject()
    private val setCitySelectionUseCase: SetCitySelectionUseCase by inject()
    private val getCityForecastUseCase: GetCityForecastUseCase by inject()

    private val paginator: OffsetPaginator<CityWeather>

    private var cityPickerRequestId: Long = 0

    init {
        subscribeToEvents()

        val callback = DefaultPaginatorCallback<CityWeather>(
            this, errorHandler, notifier, coroutineProvider.scopeMain
        ) {
            viewState.populateData(it)
        }
        paginator = OffsetPaginator(
            coroutineScope = coroutineProvider.scopeDefault,
            requestFactory = ::loadNext,
            viewController = callback
        )
    }

    private fun subscribeToEvents() {
        eventDispatcher.addEventListener(EventType.CITY_PICKER_SELECTED, this)
    }

    override fun onDestroy() {
        paginator.release()
        super.onDestroy()
    }

    override fun onEvent(eventType: EventType, data: Any?) {
        when (eventType) {
            EventType.CITY_PICKER_SELECTED -> {
                (data as? CityPickerResult)?.let {
                    if (it.requestId == cityPickerRequestId) {
                        updateCitySelection(it.city, true)
                    }
                }
            }
            else -> { }
        }
    }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        loadData()
    }

    fun onLoadNext() {
        paginator.loadNext()
    }

    private fun loadData() {
        paginator.refresh()
    }

    private suspend fun loadNext(offset: Int, limit: Int): UseCaseResult<List<CityWeather>> {
        return getSelectedCitiesForecastsUseCase(
            GetSelectedCitiesForecastsUseCase.Params(
                offset = offset.toLong(),
                limit = limit.toLong()
            )
        )
    }

    fun onCityWeatherDelete(cityWeather: CityWeather) {
        updateCitySelection(cityWeather.city, false)
    }

    fun onOpenSettings() {
        viewState.routerNavigateTo(Screens.SettingsScreen())
    }

    fun onOpenCityPicker() {
        cityPickerRequestId = System.currentTimeMillis()
        viewState.routerNavigateTo(Screens.CityPickerScreen(cityPickerRequestId))
    }

    private fun updateCitySelection(city: City, selected: Boolean) {
        val time = System.currentTimeMillis()
        coroutineProvider.scopeDefault.launch {
            setCitySelectionUseCase(
                SetCitySelectionUseCase.Params(
                    id = city.id,
                    selected = selected,
                    selectionTime = time
                )
            )
            if (selected) {
                paginator.add(CityWeather(city), paginator.currentData.size) { it.city.id == city.id }
                getCityForecastUseCase(city).process(
                    { data ->
                        paginator.removeFirst { it.city.id == city.id }
                        paginator.add(data, paginator.currentData.size) { it.city.id == city.id }
                    },
                    { error ->
                        errorHandler.proceed(error) {
                            notifier.sendMessage(it)
                        }
                    }
                )
            } else {
                paginator.removeFirst { it.city.id == city.id }
            }
        }
    }

    fun onRefresh() {
        paginator.refresh()
    }
}