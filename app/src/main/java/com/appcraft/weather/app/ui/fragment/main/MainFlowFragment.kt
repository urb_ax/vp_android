package com.appcraft.weather.app.ui.fragment.main

import android.os.Bundle
import androidx.core.os.bundleOf
import com.appcraft.weather.app.Screens
import com.appcraft.weather.app.global.ui.fragment.FlowFragment
import pro.appcraft.lib.navigation.setLaunchScreen

class MainFlowFragment : FlowFragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (childFragmentManager.fragments.isEmpty()) {
            navigator.setLaunchScreen(Screens.MainScreen())
        }
    }

    companion object {
        fun newInstance(): MainFlowFragment {
            val fragment = MainFlowFragment()
            fragment.arguments = bundleOf()
            return fragment
        }
    }
}
