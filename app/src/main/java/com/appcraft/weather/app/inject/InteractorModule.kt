@file:Suppress("PackageDirectoryMismatch")

import com.appcraft.weather.domain.interactor.city.*
import org.koin.dsl.module

val interactorModule = module {
    factory { InitCitiesUseCase(get()) }
    factory { GetCitiesUseCase(get()) }
    factory { GetCityForPositionUseCase(get()) }
    factory { SetCitySelectionUseCase(get()) }
    factory { GetSelectedCitiesForecastsUseCase(get()) }
    factory { GetCityForecastUseCase(get()) }
}
