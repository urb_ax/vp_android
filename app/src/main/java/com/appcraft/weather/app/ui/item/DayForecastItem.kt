package com.appcraft.weather.app.ui.item

import android.view.View
import com.appcraft.weather.app.R
import com.appcraft.weather.app.global.ui.BaseViewHolder
import com.appcraft.weather.app.global.utils.toTextDate
import com.appcraft.weather.domain.model.WeatherForecast
import com.mikepenz.fastadapter.items.AbstractItem
import kotlinx.android.synthetic.main.item_day_forecast.view.*
import pro.appcraft.lib.utils.common.setVisible
import java.util.*

class DayForecastItem(
    val weatherForecast: WeatherForecast
) : AbstractItem<DayForecastItem.ViewHolder>() {
    override fun getViewHolder(v: View): ViewHolder = ViewHolder(v)

    override val type = R.id.dayForecastItem

    override val layoutRes = R.layout.item_day_forecast

    class ViewHolder(view: View) : BaseViewHolder<DayForecastItem>(view) {
        override fun bindView(item: DayForecastItem, payloads: List<Any>) {
            itemView.textViewDate.text = item.weatherForecast.timestamp.toTextDate()
            item.weatherForecast.apply {
                val celsiusSuffix = getString(R.string.temperature_celsius)

                itemView.textViewTemperature.text = String.format(
                    Locale.getDefault(),
                    "%.1f %s",
                    temperatureDay,
                    celsiusSuffix
                )
                itemView.textViewInfo.setVisible()
                itemView.textViewInfo.text = String.format(
                    Locale.getDefault(),
                    "%s: %.1f %s,   %s: %.1f %s,\n%s: %.1f %s,   %s: %.1f %s\n%s: %s\n%s: %d%%\n%s: %d%%\n%s: %d %s\n%s: %.1f %s",
                    getString(R.string.morning),
                    temperatureMorning,
                    celsiusSuffix,

                    getString(R.string.day),
                    temperatureDay,
                    celsiusSuffix,

                    getString(R.string.evening),
                    temperatureEvening,
                    celsiusSuffix,

                    getString(R.string.night),
                    temperatureNight,
                    celsiusSuffix,

                    getString(R.string.weather),
                    weatherDescription,

                    getString(R.string.precipitation),
                    precipitation,

                    getString(R.string.humidity),
                    humidity,

                    getString(R.string.pressure),
                    pressure,
                    getString(R.string.pressure_mbar),

                    getString(R.string.wind_speed),
                    windSpeed,
                    getString(R.string.m_sec)
                )
            }
        }
    }
}
