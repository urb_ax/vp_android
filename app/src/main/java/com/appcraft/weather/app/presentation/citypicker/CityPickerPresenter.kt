package com.appcraft.weather.app.presentation.citypicker

import com.appcraft.weather.app.global.event.EventDispatcher
import com.appcraft.weather.app.global.notifier.Notifier
import com.appcraft.weather.app.global.notifier.SystemMessage
import com.appcraft.weather.app.global.pagination.OffsetPaginator
import com.appcraft.weather.app.global.presentation.BasePresenter
import com.appcraft.weather.app.global.presentation.DefaultPaginatorCallback
import com.appcraft.weather.app.global.presentation.ErrorHandler
import com.appcraft.weather.app.utils.CityPickerResult
import com.appcraft.weather.app.utils.EventType
import com.appcraft.weather.domain.global.CoroutineProvider
import com.appcraft.weather.domain.global.interactor.UseCaseResult
import com.appcraft.weather.domain.interactor.city.GetCitiesUseCase
import com.appcraft.weather.domain.interactor.city.GetCityForPositionUseCase
import com.appcraft.weather.domain.model.City
import com.appcraft.weather.domain.model.Position
import kotlinx.coroutines.launch
import moxy.InjectViewState
import org.koin.core.component.inject

@InjectViewState
class CityPickerPresenter : BasePresenter<CityPickerView>() {
    private val eventDispatcher: EventDispatcher by inject()
    private val errorHandler: ErrorHandler by inject()
    private val notifier: Notifier by inject()
    private val coroutineProvider: CoroutineProvider by inject()
    private val getCitiesUseCase: GetCitiesUseCase by inject()
    private val getCityForPositionUseCase: GetCityForPositionUseCase by inject()

    private val paginator: OffsetPaginator<City>

    private var requestId: Long = 0
    private var query = ""
    private var suggestedCity: City? = null

    init {
        val callback = DefaultPaginatorCallback<City>(
            this, errorHandler, notifier, coroutineProvider.scopeMain
        ) {
            viewState.populateData(it, suggestedCity)
        }
        paginator = OffsetPaginator(
            coroutineScope = coroutineProvider.scopeDefault,
            requestFactory = ::loadNext,
            viewController = callback
        )
    }

    override fun onDestroy() {
        paginator.release()
        super.onDestroy()
    }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        loadData()
    }

    fun setParams(requestId: Long) {
        this.requestId = requestId
    }

    fun onLoadNext() {
        paginator.loadNext()
    }

    fun onLoadNewList(query: String) {
        this.query = query
        loadData()
    }

    private fun loadData() {
        paginator.refresh()
    }

    private suspend fun loadNext(offset: Int, limit: Int): UseCaseResult<List<City>> {
        return getCitiesUseCase(
            GetCitiesUseCase.Params(
                offset = offset.toLong(),
                limit = limit.toLong(),
                query = query
            )
        )
    }

    fun onCitySelected(city: City) {
        if (!city.selected) {
            eventDispatcher.sendEvent(
                eventType = EventType.CITY_PICKER_SELECTED,
                data = CityPickerResult(city, requestId)
            )
            viewState.routerExit()
        }
    }

    fun onSendUserLocation(position: Position) {
        coroutineProvider.scopeMainImmediate.launch {
            viewState.showLocationProgress()
            getCityForPositionUseCase(position)
                .process(::onGetCityForPositionSuccess, ::onGetCityForPositionError)
            }
    }

    private fun onGetCityForPositionSuccess(city: City) {
        suggestedCity = city
        viewState.hideLocationProgress()
        viewState.populateData(paginator.currentData, suggestedCity)
    }

    private fun onGetCityForPositionError(throwable: Throwable) {
        viewState.hideLocationProgress()
        errorHandler.proceed(throwable) {
            notifier.sendMessage(it, SystemMessage.Level.ERROR)
        }
    }

    fun onLocationAccessDenied() {
        viewState.hideLocationProgress()
    }
}