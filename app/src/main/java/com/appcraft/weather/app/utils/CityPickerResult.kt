package com.appcraft.weather.app.utils

import com.appcraft.weather.domain.model.City

data class CityPickerResult(
    val city: City,
    val requestId: Long
)