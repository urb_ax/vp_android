package com.appcraft.weather.app.presentation.main

import com.appcraft.weather.app.Screens
import com.appcraft.weather.app.global.presentation.BasePresenter
import com.appcraft.weather.app.presentation.main.SettingsView
import moxy.InjectViewState

@InjectViewState
class SettingsPresenter : BasePresenter<SettingsView>() {
    fun onSendEmail(email: String, subject: String) {
        viewState.routerNavigateTo(Screens.ActionMailToScreen(email, subject))
    }
}