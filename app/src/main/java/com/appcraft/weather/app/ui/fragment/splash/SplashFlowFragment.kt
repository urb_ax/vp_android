package com.appcraft.weather.app.ui.fragment.splash

import android.os.Bundle
import androidx.core.os.bundleOf
import com.appcraft.weather.app.Screens
import com.appcraft.weather.app.global.ui.fragment.FlowFragment
import pro.appcraft.lib.navigation.setLaunchScreen

class SplashFlowFragment : FlowFragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (childFragmentManager.fragments.isEmpty()) {
            navigator.setLaunchScreen(Screens.SplashScreen())
        }
    }

    companion object {
        fun newInstance(): SplashFlowFragment {
            val fragment = SplashFlowFragment()
            fragment.arguments = bundleOf()
            return fragment
        }
    }
}
