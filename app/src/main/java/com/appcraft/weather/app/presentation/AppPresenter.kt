package com.appcraft.weather.app.presentation

import com.appcraft.weather.app.global.presentation.BasePresenter
import moxy.InjectViewState

@InjectViewState
class AppPresenter : BasePresenter<AppView>()