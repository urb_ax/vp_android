@file:Suppress("PackageDirectoryMismatch", "RemoveExplicitTypeArguments")

import com.appcraft.weather.data.network.CommonNetwork
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit

val apiModule = module {
    single { get<Retrofit>(named("CommonRetrofit")).create<CommonNetwork>(CommonNetwork::class.java) }
}
