package com.appcraft.weather.app.presentation.splash

import com.appcraft.weather.app.global.presentation.NavigationMvpView
import moxy.viewstate.strategy.alias.OneExecution

interface SplashView : NavigationMvpView {
    @OneExecution
    fun showDataBaseInitProgress()
}
