package com.appcraft.weather.app.ui.item

import android.view.View
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.items.AbstractItem
import com.appcraft.weather.app.R
import com.appcraft.weather.app.global.ui.BaseViewHolder
import com.appcraft.weather.app.global.utils.setTextColorRes
import com.appcraft.weather.domain.model.City
import kotlinx.android.synthetic.main.item_city_search.view.*
import java.util.*

class CitySearchItem(val city: City) : AbstractItem<CitySearchItem.ViewHolder>() {
    override fun getViewHolder(v: View): ViewHolder = ViewHolder(v)

    override val type: Int = R.id.citySearchItem

    override val layoutRes: Int = R.layout.item_city_search

    class ViewHolder(view: View) : BaseViewHolder<CitySearchItem>(view) {
        override fun bindView(item: CitySearchItem, payloads: List<Any>) {
            itemView.textViewName.text = item.city.name
            itemView.textViewInfo.text = String.format(
                Locale.getDefault(),
                "%s (lat: %.2f, lng: %.2f)",
                item.city.country,
                item.city.lat,
                item.city.lng
            )

            itemView.textViewName.setTextColorRes(
                if (item.city.selected) {
                    R.color.colorTextDark
                } else {
                    R.color.colorText
                }
            )
        }
    }
}