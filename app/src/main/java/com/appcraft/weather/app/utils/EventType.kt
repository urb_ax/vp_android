package com.appcraft.weather.app.utils

enum class EventType {
    CITY_PICKER_SELECTED
}
