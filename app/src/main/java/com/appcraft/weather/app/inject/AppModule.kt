@file:Suppress("PackageDirectoryMismatch", "USELESS_CAST")

import com.appcraft.weather.app.global.notifier.Notifier
import com.appcraft.weather.app.global.presentation.ErrorHandler
import com.appcraft.weather.domain.global.CoroutineProvider
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import pro.appcraft.lib.permissions.LocationPermissionHandler

val appModule = module {
    factory { androidContext().resources }
    single { Notifier(get()) }
    single { ErrorHandler(get()) }
    single { CoroutineProvider() }
    single { LocationPermissionHandler(androidContext()) }
}
